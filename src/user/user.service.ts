import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";

import { Model } from "mongoose";

import { User } from "./user.interface";

import { CreateUserDTO } from "./dto/create.user.dto";
import { PatchUserDto } from "./dto/patch.user.dto";

@Injectable()
export class UserService {
  constructor(@InjectModel("User") private userModel: Model<User>) {}
  // *******************user service for demo and testing proposes*******************
  async store(user: CreateUserDTO): Promise<User> {
    const newUser = new this.userModel(user);
    return newUser.save();
  }

  async findByGoogleId(googleId: string): Promise<User> {
    return await this.userModel.findOne({ googleId });
  }

  async findByEmail(email: string): Promise<User> {
    return await this.userModel.findOne({ email });
  }

  async updateById(userId: string, data: PatchUserDto): Promise<User> {
    return await this.userModel.findByIdAndUpdate(userId, data);
  }
}
