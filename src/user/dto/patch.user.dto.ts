import { CreateUserDTO } from "./create.user.dto";

export interface PatchUserDto extends Partial<CreateUserDTO> {}
