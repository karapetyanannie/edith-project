export class CreateUserDTO {
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly password?: string;
  readonly googleId?: string;
  readonly refreshToken?: string;
}
