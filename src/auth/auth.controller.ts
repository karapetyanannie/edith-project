import { Controller, Get, Req, UseGuards } from "@nestjs/common";

import { AuthService } from "./auth.service";

import { GoogleAuthGuard } from "./guards/google-auth.guard";

@Controller("auth")
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(GoogleAuthGuard)
  @Get("google")
  async signInWithGoogle() {}

  @UseGuards(GoogleAuthGuard)
  @Get("google/redirect")
  async signInWithGoogleRedirect(@Req() req) {
    return this.authService.signInWithGoogle(req);
  }
}
